//
//  UIImageView+Downloader.swift
//  TT
//
//  Created by Oleg Mytsovda on 08.05.18.
//  Copyright © 2018 Oleg Mytsovda. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func downloadImage(url: URL, contentMode mode: UIViewContentMode) {
        let sessionConfiguration = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfiguration)
        
        let request = NSMutableURLRequest(url: url, cachePolicy: NSURLRequest.CachePolicy.returnCacheDataElseLoad, timeoutInterval: 60)
        request.httpShouldHandleCookies = false
        request.httpMethod = "GET"
        
        let cachedResponse = URLCache.shared.cachedResponse(for: request as URLRequest)
        
        if let data = cachedResponse?.data {
            DispatchQueue.main.async { () -> Void in
                self.contentMode = mode
                self.image = UIImage(data: data)
            }
        } else {
            session.dataTask(with: request as URLRequest, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
                DispatchQueue.main.async() { () -> Void in
                    if let data = data {
                        DispatchQueue.main.async { () -> Void in
                            self.contentMode = mode
                            self.image = UIImage(data: data)
                        }
                    } else if let error = error?.localizedDescription {
                        NSLog("Failed to download image")
                    } else {
                        NSLog("Failed to download image")
                    }
                }
            }).resume()
        }
    }
    
    
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async { () -> Void in
                self.contentMode = mode
                self.image = image
            }
            }.resume()
    }
    
    func downloadedFrom(link: String, mode: UIViewContentMode) {
        guard let url = URL(string: link) else { return }
        downloadImage(url: url, contentMode: mode)
    }
}
