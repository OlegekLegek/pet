//
//  UserCell.swift
//  TT
//
//  Created by Oleg Mytsovda on 08.05.18.
//  Copyright © 2018 Oleg Mytsovda. All rights reserved.
//

import UIKit

class UserCell: UITableViewCell {

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var profileLinkLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        self.avatarImageView.image = nil
        self.profileLinkLabel.text = nil
        self.loginLabel.text = nil
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
