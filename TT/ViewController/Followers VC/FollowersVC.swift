//
//  FollowersVC.swift
//  TT
//
//  Created by Oleg Mytsovda on 08.05.18.
//  Copyright © 2018 Oleg Mytsovda. All rights reserved.
//

import UIKit
import SVProgressHUD

class FollowersVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var page = 1
        
    var user = User()
    var userLists:[User] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "\(user.login!.capitalized) Followers"
        fetchData()
    }

    func fetchData() {
        SVProgressHUD.show()
        user.getFolovers(self.page) { (follovers, error) in
            SVProgressHUD.dismiss()
            if let err = error {
                SVProgressHUD.showError(withStatus: err)
            } else {
                if let lists = follovers {
                    self.userLists.append(contentsOf: lists)
                    self.tableView.reloadData()
                } else {
                    SVProgressHUD.showError(withStatus: "No more users")
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
