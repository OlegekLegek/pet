//
//  FollowersTableMethodDelegate.swift
//  TT
//
//  Created by Oleg Mytsovda on 08.05.18.
//  Copyright © 2018 Oleg Mytsovda. All rights reserved.
//

import UIKit

extension FollowersVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.userLists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "userCell", for: indexPath) as! UserCell
        
        let currentUser = self.userLists[indexPath.row]
        parseValueForCell(cell, user: currentUser)
        
        if self.userLists.count - 2 == indexPath.row {
            fetchData()
        }
        
        return cell
    }
    
    func parseValueForCell(_ cell:UserCell, user: User) {
        cell.loginLabel.text = user.login
        cell.profileLinkLabel.text = user.profileLink
        if let photoStrURL = user.photoUrl {
            cell.avatarImageView.downloadedFrom(link: photoStrURL, mode: .scaleAspectFill)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
