//
//  ViewController.swift
//  TT
//
//  Created by Oleg Mytsovda on 07.05.18.
//  Copyright © 2018 Oleg Mytsovda. All rights reserved.
//

import UIKit
import SVProgressHUD

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var sinceID = 0
    var userLists:[User] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Users"
        fetchData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func fetchData() {
        SVProgressHUD.show()
        User.getUsers(sinceID) { (users, lastID, error) in
            SVProgressHUD.dismiss()
            if let err = error {
                SVProgressHUD.showError(withStatus: err)
            } else {
                if let lists = users {
                    self.userLists.append(contentsOf: lists)
                    self.sinceID = lastID!
                    self.tableView.reloadData()
                } else {
                    SVProgressHUD.showError(withStatus: "No more users")
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "followers" {
            let followers = segue.destination as! FollowersVC
            followers.user = sender as! User
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


}

