//
//  RequestManager.swift
//  TT
//
//  Created by Oleg Mytsovda on 08.05.18.
//  Copyright © 2018 Oleg Mytsovda. All rights reserved.
//

import Foundation
import UIKit

class RequestManager {
    func sessionRequest(data: NSDictionary, to: String, method: String, completionHandler: @escaping (NSArray?, Error?) -> Void) -> URLSessionDataTask {
        let urlString = "https://api.github.com/\(to)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let url = URL(string: urlString)
        print(url!)
        var request = URLRequest(url:url!)
        request.httpMethod = method

        if data != [:] {
            request.httpBody = try! JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
        }
        
        let task = URLSession.shared.dataTask(with: request) { (data, _, error) in
            if let error = error {
                if error.localizedDescription == "cancelled" {
                    print("cancelled request")
                } else {
                    print("error=\(String(describing: error))")
                    OperationQueue.main.addOperation {
                        completionHandler(nil, error)
                    }
                }
            } else {
                do {
                    let result = try JSONSerialization.jsonObject(with: data!, options: []) as? NSArray
                    OperationQueue.main.addOperation {
                        completionHandler(result, nil)
                    }
                } catch {
                    print(error)
                    OperationQueue.main.addOperation {
                        completionHandler(nil, error)
                    }
                }
            }
        }
        task.resume()
        return task
    }
    
}
