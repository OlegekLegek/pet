//
//  User.swift
//  TT
//
//  Created by Oleg Mytsovda on 07.05.18.
//  Copyright © 2018 Oleg Mytsovda. All rights reserved.
//

import Foundation
import RealmSwift

class User: Object {
    
    @objc dynamic var id = Int()
    @objc dynamic var login:String?
    @objc dynamic var profileLink: String?
    @objc dynamic var photoUrl: String?
    @objc dynamic var foloversLink: String?

    override static func primaryKey() -> String? {
        return "id"
    }
    
    static func toRealm(data: NSDictionary) -> User {
        let realm = try! Realm()
        
        let id = data["id"] as! Int
        var object: User
        if let optional = realm.objects(User.self).filter("id = \(id)").first {
            object = optional
        } else {
            object = User()
            object.id = id
        }
        
        try! realm.write {
            
            if let log = data["login"] as? String {
                object.login = log
            }
            if let profileLink = data["html_url"] as? String {
                object.profileLink = profileLink
            }
            if let photoURL = data["avatar_url"] as? String {
                object.photoUrl = photoURL
            }
            if let foloversLink = data["followers_url"] as? String {
                object.foloversLink = foloversLink
            }
            
        }
       
        return object
    }
    
    static func getUsers(_ fromId:Int, completionHandler: @escaping ([User]?, Int?, String?) -> Void) {
        _ = RequestManager().sessionRequest(data: [:] as NSDictionary, to: "users?per_page=10&since=\(fromId)", method: "GET", completionHandler: { (result, error) in
            if let res = result as? NSArray {
                var usersArray = [User]()
                for userObject in res {
                    let user = User.toRealm(data: userObject as! NSDictionary)
                    usersArray.append(user)
                }
                completionHandler(usersArray,usersArray.last?.id, nil)
            } else if let err = error {
                completionHandler(nil, nil, err.localizedDescription)
            } else {
                completionHandler(nil, nil, "Unexpected error")
            }
        })
    }
    
    func getFolovers(_ page:Int, completionHandler: @escaping ([User]?, String?) -> Void) {
        _ = RequestManager().sessionRequest(data: [:] as NSDictionary, to: "users/\(self.login!)/followers?per_page=10&page=\(page)", method: "GET", completionHandler: { (result, error) in
            if let res = result as? NSArray {
                var usersArray = [User]()
                for userObject in res {
                    let user = User.toRealm(data: userObject as! NSDictionary)
                    usersArray.append(user)
                }
                completionHandler(usersArray, nil)
            } else if let err = error {
                completionHandler(nil, err.localizedDescription)
            } else {
                completionHandler(nil, "Unexpected error")
            }
        })
    }
}
